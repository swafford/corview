from ete3 import *
import argparse
import os, fileinput
import pandas
import numpy as np
from shutil import copyfile

def tipState(tipStateF,fName,output,colors,labels):
    tipStates = pandas.read_csv(tipStateF,sep=',',header=0)
    outESvis = os.path.join(output,fName.split(".")[0]+"_extColors.txt")
    copyfile("colored_strips_template.txt",outESvis)
    y=0
    for x in labels:
        tipStates[tipStates == x] = colors[y]
        y+=1
    with open(outESvis,'a') as outfile:
        outfile.write('\n')
        np.savetxt(outfile,tipStates,fmt='%s',delimiter=',',newline='\n')
    with open(outESvis,'r') as outfile:
        filedata = outfile.read()
        filedata = filedata.replace("$legendColors",",".join(colors))\
                    .replace("$legendLabel",",".join(labels))\
                    .replace("$fieldColors",",".join(colors))\
                    .replace("$fieldLabels",",".join(labels))\
                    .replace("$legendShapes",",".join(['1']*len(colors)))
    with open(outESvis,'w') as outfile:
        outfile.write(filedata)

def internalState(intStateF,tips,fName,output,colors):
    outISvis = os.path.join(output,fName.split(".")[0]+"_intNode.txt")
    intStates = pandas.read_csv(intStateF,sep=',',header=0)
    try:
        intStates['Unnamed: 0'] = intStates['Unnamed: 0']+tips
        intStates['Unnamed: 0'] = "INT"+intStates['Unnamed: 0'].astype(str)
    except:
        print("Error locating 'Unnamed' sequences.  Please make sure this is correct")
    copyfile("pie_template.txt",outISvis)
    intStates.insert(1,'size',2)
    intStates.insert(1,'position',0.5)
    with open(outISvis,'a') as outfile:
        outfile.write("\n")
        np.savetxt(outfile,intStates.values,fmt='%s',delimiter=",")
    with open(outISvis,'r') as outfile:
        filedata = outfile.read()
        filedata = filedata.replace("$legendColors",",".join(colors[:len(internal_labels)]))\
                    .replace("$legendLabel",",".join(internal_labels))\
                    .replace("$fieldColors",",".join(colors[:len(internal_labels)]))\
                    .replace("$fieldLabels",",".join(internal_labels))\
                    .replace("$legendShapes",",".join(['1']*len(internal_labels)))
    with open(outISvis,'w') as outfile:
        outfile.write(filedata)
    print("File named '%s' has been written to: %s"%(fName.split(".")[0]+"_intNode.txt",output))

def inteRnalize(treeFile,output = "internal_R_nodes.nwk",outgroup=None):
    tree = Tree(treeFile,format=1)
    if outgroup != None:
        ancestor = tree.get_common_ancestor(outgroup[0],outgroup[1])
        tree.set_outgroup(ancestor)
    o_name = os.path.basename(treeFile).split(".")[0]+"_Rnodes.nwk"
    output2 = o_name
    i = len([x for x in tree.iter_leaves()])
    rooted = False
    for node in tree.traverse(strategy='preorder'):
        if node.is_leaf():
            continue
        i+=1
        if node.is_root()==False and node.is_leaf()==False:
            node.name = "INT"+str(i)
        elif node.is_root():
            if len(node.children) == 2:
                rooted = node.name
        else:
            print "The MRCA of these nodes is odd, please inspect: ",[x.name for x in node.get_children()]
    x = tree.write(features=['alrt','abys','UFB']).replace(";","%s;"%rooted)
    if rooted:
        with open(output2,'w') as outfile:
            outfile.write(x)
    else:
        tree.write(outfile=output2,features=['alrt','abys','UFB'],format=1)
    print("File named '%s' has been written to: %s"%(o_name,output2))
    return([tree,len([x for x in tree.iter_leaves()])])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Get internal node labels that R would use.")
    parser.add_argument('-t', metavar = 'TreeFile', type = str, required = True,
                        help = "Tree File in Newick Format")
    parser.add_argument('-nn', metavar = 'Internal node states', type = str, required = False,
                        help = "Internal node states in csv format")
    parser.add_argument('-tn', metavar = 'Internal node states', type = str, required = False,
                        help = "Tip node states in csv format")
    parser.add_argument('-o', metavar = 'Output', type = str, required = False,
                        default = ".", help = "Directory for output to be written to")
    args = parser.parse_args()
    tip_labels = ['0','1','0&1']
    internal_labels = ['0','1']
    colors0 = "#000000"
    colors1 = "#C62E65"
    colors2 = "#2d41c4"
    colors = [colors0,colors1,colors2]
    treefile = args.t
    out = os.path.abspath(args.o)
    base = os.path.basename(treefile)
    newtree,intNodes = inteRnalize(treefile,out)
    if args.tn:
        tipStateF = args.tn
        tipState(tipStateF,base,out,colors,tip_labels)
    if args.nn:
        stateFile = args.nn
        internalState(stateFile,intNodes,base,out,colors)
